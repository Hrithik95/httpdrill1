const http = require('http');
const port = process.env.PORT || 8000;
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');


function requestHandler(request, response) {
    const { url } = request;
    if (url === '/html') {

        fs.readFile('./index.html', 'utf-8', function (error, data) {
            if (error) {
                response.writeHead(500, { 'content-type': 'text/html' });
                return response.end('<h1>500:Internal Server Error</h1>');
            } else {
                response.writeHead(200, { 'content-type': 'text/html' });
                console.log(response);
                return response.end(data);
            }

        });
    } else if (url === '/json') {
        console.log(request.url);
        response.writeHead(200, { 'content-type': 'text/json' });

        fs.readFile('./data.json', 'utf-8', function (error, data) {
            if (error) {
                response.writeHead(500, { 'content-type': 'text/html' });
                return response.end('<h1>500:Internal Server Error</h1>');
            } else {
                response.writeHead(200, { 'content-type': 'text/json' });
                return response.end(data);
            }

        });
    } else if (url === '/uuid') {
        console.log(request.url);
        response.writeHead(200, { 'content-type': 'text/json' });
        let uuidData = JSON.stringify({
            uuid: uuidv4()
        });
        response.end(uuidData);
    } else if (request.url.startsWith("/status")) {
        const statusCode = Number(request.url.split("/")[2]);
        if (statusCode >= 100 && statusCode < 200) {
            response.end("Enter status greater than 199");
        } else {
            response.end(`Responding with status-code:${statusCode}`);
        }
    } else if (request.url.startsWith("/delay/")) {
        const delay = Number(request.url.split("/")[2]);
        setTimeout(() => {
            response.writeHead(200, { 'content-type': 'text/plain' });
            response.end(`Getting the response after ${delay} seconds`);
        }, delay * 1000);
    } else {
        response.writeHead(404, { "content-type": "text/html" });
        response.end("<h1>404:Page Not Found</h1>");
    }
}

const server = http.createServer(requestHandler);

server.listen(port, function (error) {
    if (error) {
        console.log(error);
        return;
    } else {
        console.log("Server is up and running on port:", port);
    }

});